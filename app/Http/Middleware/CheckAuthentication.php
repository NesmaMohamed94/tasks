<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use App\Http\Controllers\Users\Model\Users;
use Auth;
class CheckAuthentication
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $data = $this->Check($request);

        if ($data == false) {
            return response()->json(['message' => "UnAuthorized for this Action"], 401);
        }else{
            return $next($request);
        }

    }
    public function Check($request)
    {
        $user = Users::where("id", Auth::id())->first();
        
        if(intval($request->segment(4)) == $user->id){
            return true;
        }else{
            return false;
        }
    }

}
