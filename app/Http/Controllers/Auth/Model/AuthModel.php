<?php

namespace App\Http\Controllers\Auth\Model;

use Illuminate\Database\Eloquent\Model;

class AuthModel extends Model
{
    //
     /**
     * The database table used by the model.
     *
     * @var string
     */
    public $table = 'oauth_clients';

}
