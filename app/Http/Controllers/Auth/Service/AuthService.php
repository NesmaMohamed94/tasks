<?php

namespace App\Http\Controllers\Auth\Service;
use App\Http\Controllers\Auth\Model\AuthModel;

class AuthService
{
    //
    /**
     * Get Secret key fo user function
     *
     * @param int $id
     * @return void
     */
    public function secret(int $id){
        return AuthModel::find($id);
    }
}
