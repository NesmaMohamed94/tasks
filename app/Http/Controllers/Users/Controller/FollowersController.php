<?php

namespace App\Http\Controllers\Users\Controller;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Users\Service\FollowersService;
use Auth;
use Illuminate\Http\Request;
use Validator;

class FollowersController extends Controller
{
    private $service;

    public function __construct()
    {
        $this->service = new FollowersService;
    }

    /**
     * Display a listing of all followed users.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(!Auth::check()){
            return $this->error401();
        }
        return $this->Success200($this->service->paginate());
    }

    /**
     * Follow User.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = validator(['follower_id' => $request->get("follower_id")], [
            'follower_id' => 'required|exists:users,id|numeric',
        ]);

        if ($validator->fails()) {
            return $this->error400(['errors' => $validator->errors()]);
        }

        $userData['user_id'] = Auth::id();
        $userData['follower_id'] = $request->get("follower_id");
        if($userData['user_id'] == $userData['follower_id'] ){
            return $this->error400(['errors' => "You can't follow your self"]);
        }
        $check = $this->service->checkIfFollowed($userData['user_id'],$userData["follower_id"]);
        if($check > 0 ){
            return $this->error400(['errors' => "You are following this user"]);
        }
        $this->service->create($userData);
        return $this->Success201();
    }

    /**
     * Display the specified Follower.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if(!Auth::check()){
            return $this->error401();
        }
        $validator = validator(['id' => $id], [
            'id' => 'required|exists:followers,id|numeric',
        ]);
        if ($validator->fails()) {
            return $this->error400(['errors' => $validator->errors()]);
        }

        return $this->Success200(["data" => $this->service->getFollowerByID($id)]);
    }

    /**
     * Unfollow user.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(!Auth::check()){
            return $this->error401();
        }
        $validator = validator(['id' => $id], [
            'id' => 'required|exists:followers,id|numeric',
        ]);
        if ($validator->fails()) {
            return $this->error400(['errors' => $validator->errors()]);
        }

        $this->service->softDelete($id);
        return $this->Success202();
    }


}
