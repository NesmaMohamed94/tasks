<?php

namespace App\Http\Controllers\Users\Controller;

use App\Http\Controllers\Auth\Service\AuthService;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Users\Service\UserService;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Validator;

class UsersController extends Controller
{
    private $service;

    public function __construct()
    {
        $this->service = new UserService;
        $this->Authservice = new AuthService;

    }

    /**
     * Login function
     *
     * @param Request $request
     * @return void
     */
    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|string|email|max:255|',
            'password' => 'required',
        ]);

        if ($validator->fails()) {
            return $this->error400(['errors' => $validator->errors()]);
        }

        $credentials = $request->only("email", "password");
        if (!Auth::attempt($credentials)) {
            return $this->error401();
        }

        $user = $this->service->getUserByEmail($credentials['email']);
        $oauthClient = $this->Authservice->secret($user->auth_client_id);
        if (!$oauthClient) {
            return $this->error401();
        }

        $request->request->add([
            'username' => $request->email,
            'password' => $request->password,
            'grant_type' => 'password',
            'client_id' => $user->auth_client_id,
            'client_secret' => $oauthClient->secret,
            'scope' => '',
        ]);
        $tokenRequest = Request::create(
            '/oauth/token',
            'post'
        );
        $response = \Route::dispatch($tokenRequest);
        if ($response->getStatusCode() == 200) {
            $userToken = json_decode($response->getContent(), true);
            return $this->Success200([
                "user_token" => $userToken,
                "user" => $user,

            ]);
        }

        return $response;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|string|email|max:255|unique:users',
            'name' => 'required|max:255',
            'password' => 'required|min:8',
            'image' => 'required|image',
        ]);

        if ($validator->fails()) {
            return $this->error400(['errors' => $validator->errors()]);
        }

        $userData = $request->all();
        $file = $request->file('image');
        if (!empty($file)) {
            $image = $file->getClientOriginalName();
            Storage::disk("images")->put($image, File::get($file));
            $userData['image'] = "http://localhost/tasks/public/images/" . $file->getClientOriginalName();
        }
        $userData['auth_client_id'] = 2;
        $userData['password'] = bcrypt($request->password);
        $this->service->create($userData);
        return $this->Success201();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (!Auth::check()) {
            return $this->error401();
        }
        $validator = validator(['id' => $id], [
            'id' => 'required|exists:users,id|numeric',
        ]);
        if ($validator->fails()) {
            return $this->error400(['errors' => $validator->errors()]);
        }

        return $this->Success200(["data" => $this->service->getUserByID($id)]);
    }

    /**
     * Update User Data in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (!Auth::check()) {
            return $this->error401();
        }
        // validator
        $validator = validator(['id' => $id], [
            'id' => 'required|exists:users,id|numeric',
        ]);
        if ($validator->fails()) {
            return $this->error400(['errors' => $validator->errors()]);
        }
        $userData = $request->all();
        if (isset($userData['password'])) {
            $userData['password'] = bcrypt($request->get("password"));
        }
        $file = $request->file('image');
        if (!empty($file)) {
            $image = $file->getClientOriginalName();
            Storage::disk("images")->put($image, File::get($file));
            $userData['image'] = $file->getClientOriginalName();
        }

        $this->service->updateUser($userData, $id);
        return $this->Success202();

    }

    /**
     * Remove User from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (!Auth::check()) {
            return $this->error401();
        }
        $validator = validator(['id' => $id], [
            'id' => 'required|exists:users,id|numeric',
        ]);
        if ($validator->fails()) {
            return $this->error400(['errors' => $validator->errors()]);
        }

        $this->service->softDelete($id);
        return $this->Success202();
    }

    /**
     * Getting User timeline function
     *
     * @return \Illuminate\Http\Response
     */
    public function timeline(Request $request)
    {
        if (!Auth::check()) {
            return $this->error401();
        }
        $validator = Validator::make($request->all(),
            ['lang' => 'required|string|in:en,ar']);
        if ($validator->fails()) {
            return $this->error400(['errors' => $validator->errors()]);
        }

        $lang = $request->get("lang");
        $id = Auth::id();
        return $this->Success200($this->service->timeline($id, $lang));
    }

}
