<?php

namespace App\Http\Controllers\Users\Model;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Followers extends Model
{
    use SoftDeletes;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    public $table = 'followers';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [
        'user_id', 'follower_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
    ];

    public function user()
    {
        return $this->belongsTo('App\Http\Controllers\Users\Model\Users', 'user_id');
    }
    public function followerData()
    {
        return $this->belongsTo('App\Http\Controllers\Users\Model\Users', 'follower_id');
    }
}
