<?php

namespace App\Http\Controllers\Users\Service;

use App\Http\Controllers\Users\Model\Followers;
use Auth;
class FollowersService
{

    /**
     * Follow User function
     *
     * @param array $userData
     * @return void
     */

    public function create(array $userData)
    {
        return Followers::create($userData);

    }
    public function checkIfFollowed(int $id,int $follower){
        return Followers::where("user_id",$id)->where("follower_id",$follower)->count();
    }
    /**
     * Get single Follower data by ID function
     *
     * @param integer $id
     * @return void
     */
    public function getFollowerByID(int $id)
    {
        return Followers::where("id", $id)->with(['followerData' => function ($q) {
            $q->select("id", "name");
        },
        ])->first();

    }

    /**
     * Return all followers by this user with pagination function
     *
     * @return void
     */
    public function paginate()
    {
        return Followers::with(['user' => function ($q) {
            $q->select("id", "name","image");
        }, 'followerData' => function ($q) {
            $q->select("id", "name","image");
        },
        ])->where("user_id",Auth::id())->paginate(10);

    }

    /**
     * Delete user but not permenantly function
     *
     * @param integer $id
     * @return void
     */
    public function softDelete($id)
    {
        Followers::find($id)->delete();
    }
}
