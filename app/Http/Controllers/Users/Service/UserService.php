<?php

namespace App\Http\Controllers\Users\Service;

use App\Http\Controllers\Tweets\Model\Tweets;
use App\Http\Controllers\Users\Model\Followers;
use App\Http\Controllers\Users\Model\Users;

class UserService
{

    /**
     * Create New User function
     *
     * @param array $userData
     * @return void
     */

    public function create(array $userData)
    {
        return Users::create($userData);

    }

    /**
     * Get single user date by email function
     *
     * @param string $email
     * @return void
     */
    public function getUserByEmail(string $email)
    {
        return Users::where("email", $email)->first();

    }

    /**
     * Get single user data by ID function
     *
     * @param integer $id
     * @return void
     */
    public function getUserByID(int $id)
    {
        return Users::where("id", $id)->first();

    }

    /**
     * Update User Data by id function
     *
     * @param array $userData
     * @param integer $id
     * @return void
     */
    public function updateUser(array $userData,int $id)
    {
        Users::where("id", $id)->update($userData);
        return $user = Users::where("id", $id)->first();

    }

    /**
     * Return User Timeline function
     *
     * @return void
     */
    public function timeline(int $id, string $lang)
    {
        if ($lang == "en") {
            $desc = "english_description";
        } else {
            $desc = "arabic_description";

        }
        $users = array();
        array_push($users, $id);
        $followers = Followers::where("user_id", $id)->get(["follower_id"]);
        foreach ($followers as $follower) {
            if (!in_array($follower->follower_id, $users)) {
                array_push($users, $follower->follower_id);
            }
        }

        return Tweets::select("id", $desc . " as description", "user_id", "created_at")
                ->with(['user' => function ($q) {
                        $q->select("id", "name", "image");
                    }])
                ->where($desc, "!=", null)
                ->whereIn("user_id", $users)->paginate();
    }

    /**
     * Delete user function
     *
     * @param integer $id
     * @return void
     */
    public function softDelete(int $id)
    {
        Users::find($id)->delete();
    }

}
