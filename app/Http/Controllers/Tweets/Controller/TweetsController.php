<?php

namespace App\Http\Controllers\Tweets\Controller;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Tweets\Service\TweetsService;
use Auth;
use Illuminate\Http\Request;
use Validator;

class TweetsController extends Controller
{
    private $service;
    public function __construct()
    {
        $this->service = new TweetsService;

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        $validator = Validator::make($request->all(), [
            'lang' => 'required|string',
        ]);
        if ($validator->fails()) {
            return $this->error400(['errors' => $validator->errors(), 'message' => "Bad params.", "status" => 400]);
        }
        $lang = $request->get("lang");
        $data = $this->service->paginate($lang);
        return $this->Success200(['data' => $data, "message" => "Retrieved Succesfully"]);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        if (!Auth::check()) {
            return $this->error401();
        }
        $validator = Validator::make($request->all(), [
            'description' => 'required|string|max:140',
            'lang' => 'required|string',
        ]);
        if ($validator->fails()) {
            return $this->error400(['errors' => $validator->errors(), 'message' => "Bad params.", "status" => 400]);
        }

        $data = $request->all();
        $user_id = Auth::id();
        $this->service->create($data, $user_id);
        return $this->Success202();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        //
        $validator = Validator::make($request->all(),
            ['lang' => 'required|string|in:en,ar'],
            ['id' => $id], [
            'id' => 'required|exists:tweets,id|numeric',
        ]);
        if ($validator->fails()) {
            return $this->error400(['errors' => $validator->errors()]);
        }
        $lang = $request->get("lang");
        $data = $this->service->TweetsByID($lang, $id);
        return $this->Success200(['data' => $data]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        if (!Auth::check()) {
            return $this->error401();
        }
        $validator =  Validator::make($request->all(),
        ['lang' => 'required|string|in:en,ar',
        "description"=>"required|string|max:140"],
        ['id' => $id], [
        'id' => 'required|exists:tweets,id|numeric',
    ]);
        if ($validator->fails()) {
            return $this->error400(['errors' => $validator->errors()]);
        }

        $data = $request->all();
        $this->service->updateTweets($data, $id);
        return $this->Success202();
    }

    /**
     * Remove tweet from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        if (!Auth::check()) {
            return $this->error401();
        }
        $validator = validator(['id' => $id], [
            'id' => 'required|exists:tweets,id|numeric',
        ]);
        if ($validator->fails()) {
            return $this->error400(['errors' => $validator->errors()]);
        }

        $this->service->softDelete($id);
        return $this->Success202();

    }
}
