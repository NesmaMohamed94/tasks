<?php

namespace App\Http\Controllers\Tweets\Service;

use App\Http\Controllers\Tweets\Model\Tweets;

class TweetsService
{
    //
    /**
     * Create Tweet function
     *
     * @param array $data
     * @param string $lang
     * @param int $user_id
     * @return void
     */
    public function create(array $data, int $user_id)
    {
        if ($data['lang'] == "en") {
            $newData['english_description'] = $data['description'];
        } else {
            $newData['arabic_description'] = $data['description'];
        }
        $newData['user_id'] = $user_id;
        return Tweets::create($newData);
    }

    /**
     * Get Tweet Request By ID function
     *
     * @param int $id
     * @return void
     */
    public function TweetsByID(string $lang, int $id)
    {
        if ($lang == "en") {
            $desc = "english_description";
        } else {
            $desc = "arabic_description";
        }
        return Tweets::select("id", $desc . " as description", "user_id", "created_at")
            ->with(['user' => function ($q) {
                $q->select("id", "name", "image");
            }])
            ->where($desc, "!=", null)->where("id", $id)->first();
    }
    /**
     * Get Tweets with Pagination function
     *@param string $lang
     * @return void
     */
    public function paginate(string $lang)
    {
        if ($lang == "en") {
            $desc = "english_description";
        } else {
            $desc = "arabic_description";
        }
        return Tweets::select("id", $desc . " as description", "user_id", "created_at")
            ->with(['user' => function ($q) {
                $q->select("id", "name", "image");
            }])->where($desc, "!=", null)->paginate(10);

    }
    /**
     * Update Tweet Data function
     *
     * @param array $data
     * @param int $id
     * @return void
     */
    public function updateTweets(array $data, int $id)
    {
        if ($data['lang'] == "en") {
            $newData['english_description'] = $data['description'];
        } else {
            $newData['arabic_description'] = $data['description'];
        }
        Tweets::where("id", $id)->update($newData);
    }
    /**
     * SoftDelete Tweets function
     *
     * @param int $id
     * @return void
     */
    public function softDelete(int $id)
    {
        Tweets::find($id)->delete();

    }

}
