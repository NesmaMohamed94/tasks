<?php

namespace App\Http\Controllers\Tweets\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Tweets extends Model
{
    //
    use SoftDeletes;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    public $table = 'tweets';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = ['id',"english_description" ,"arabic_description", "user_id"];

    public function user()
    {
        return $this->belongsTo('App\Http\Controllers\Users\Model\Users', 'user_id');
    }
    public function followers()
    {
        return $this->belongsTo('App\Http\Controllers\Users\Model\Followers', 'user_id');
    }
}
