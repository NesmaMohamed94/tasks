<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */

/* Users Routes */

Route::resource('users', 'App\Http\Controllers\Users\Controller\UsersController');
Route::post("/login", ['as' => 'login', 'uses' => "App\Http\Controllers\Users\Controller\UsersController@login"]);

/* End Users Route */

Route::middleware('auth:api')->group(function () {
/* Timeline */
    Route::get('timeline', 'App\Http\Controllers\Users\Controller\UsersController@timeline');
/* End Timeline */
    /*Tweets Routes */

    Route::resource('tweets', 'App\Http\Controllers\Tweets\Controller\TweetsController');
    Route::post('tweets/update/{id}', 'App\Http\Controllers\Tweets\Controller\TweetsController@update');

    /*End Tweets Routes */

    /*Follow User Routes */

    Route::resource('follow', 'App\Http\Controllers\Users\Controller\FollowersController');

    /*End Follow User Routes */

    Route::middleware("check-authentication")->group(function () {
        /* Users Routes */
        Route::post('users/update/{id}', 'App\Http\Controllers\Users\Controller\UsersController@update');
        Route::get('users/destroy/{id}', 'App\Http\Controllers\Users\Controller\UsersController@destroy');
        /* End Users Route */
    });
});
