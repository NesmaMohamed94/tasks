-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 23, 2020 at 05:05 PM
-- Server version: 10.4.14-MariaDB
-- PHP Version: 7.4.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `tasks`
--

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `followers`
--

CREATE TABLE `followers` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `follower_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `followers`
--

INSERT INTO `followers` (`id`, `user_id`, `follower_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(2, 1, 2, '2020-09-23 10:56:11', '2020-09-23 10:56:16', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2016_06_01_000001_create_oauth_auth_codes_table', 1),
(4, '2016_06_01_000002_create_oauth_access_tokens_table', 1),
(5, '2016_06_01_000003_create_oauth_refresh_tokens_table', 1),
(6, '2016_06_01_000004_create_oauth_clients_table', 1),
(7, '2016_06_01_000005_create_oauth_personal_access_clients_table', 1),
(8, '2019_08_19_000000_create_failed_jobs_table', 1),
(9, '2020_09_22_105813_add_image_to_users_table', 2),
(10, '2020_09_22_105834_add_soft_delete_to_users_table', 2),
(11, '2020_09_22_115210_add_auth_client_id_to_users_table', 3),
(13, '2020_09_22_132335_create_table_tweets', 4),
(14, '2020_09_22_201828_create_table_followers', 5);

-- --------------------------------------------------------

--
-- Table structure for table `oauth_access_tokens`
--

CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_access_tokens`
--

INSERT INTO `oauth_access_tokens` (`id`, `user_id`, `client_id`, `name`, `scopes`, `revoked`, `created_at`, `updated_at`, `expires_at`) VALUES
('3c952f013e182bc0ed3f1efeba2e4e575a122c9662913a3ff24d2a79fe2f90631a5faa11fa1a04f8', 1, 2, NULL, '[]', 0, '2020-09-22 10:06:37', '2020-09-22 10:06:37', '2021-09-22 12:06:37'),
('4f1f04610fd69b665fe1b80ff77108fe6e1e50a445e3d72ee8cd575273664153adb1bada0790af0a', 1, 2, NULL, '[]', 0, '2020-09-22 18:45:22', '2020-09-22 18:45:22', '2021-09-22 20:45:22'),
('a966707f56445f19df4fd21f1e54a7e0256ef030348480b074ecb7626f5fe37cdb47acb2d3e36d8e', 1, 2, NULL, '[]', 0, '2020-09-22 09:54:34', '2020-09-22 09:54:34', '2021-09-22 11:54:34'),
('b1ebc0516381989b0f0aeac9af151e37876258178966e178fc07e9d4b8fa43f5c9a480879bbaaa28', 1, 2, NULL, '[]', 0, '2020-09-22 09:57:18', '2020-09-22 09:57:18', '2021-09-22 11:57:18'),
('b6bf2e11c6812c6a5b1f3e6ed1fbf9f561afc59a17f46485cd22fd253c94b74055b34e180293eaf0', 1, 2, NULL, '[]', 0, '2020-09-23 10:39:45', '2020-09-23 10:39:45', '2021-09-23 12:39:45'),
('d3b83892aeedd451d391762fee30e75a7115929595d00526f3d18ee3713a2eb9ccf4faffc1ed9e51', 1, 2, NULL, '[]', 0, '2020-09-22 11:10:45', '2020-09-22 11:10:45', '2021-09-22 13:10:45');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_auth_codes`
--

CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_clients`
--

CREATE TABLE `oauth_clients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provider` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_clients`
--

INSERT INTO `oauth_clients` (`id`, `user_id`, `name`, `secret`, `provider`, `redirect`, `personal_access_client`, `password_client`, `revoked`, `created_at`, `updated_at`) VALUES
(1, NULL, 'Laravel Personal Access Client', 'Shbyjb2joqHYbjNmOaiCZq2DkPct12P32glc1Nrz', NULL, 'http://localhost', 1, 0, 0, '2020-09-22 08:47:40', '2020-09-22 08:47:40'),
(2, NULL, 'Laravel Password Grant Client', 'zOa2gw8P9XQonsLm8bks9FDZCeDiMycExsHxK6jx', 'users', 'http://localhost', 0, 1, 0, '2020-09-22 08:47:40', '2020-09-22 08:47:40');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_personal_access_clients`
--

CREATE TABLE `oauth_personal_access_clients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_personal_access_clients`
--

INSERT INTO `oauth_personal_access_clients` (`id`, `client_id`, `created_at`, `updated_at`) VALUES
(1, 1, '2020-09-22 08:47:40', '2020-09-22 08:47:40');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_refresh_tokens`
--

CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_refresh_tokens`
--

INSERT INTO `oauth_refresh_tokens` (`id`, `access_token_id`, `revoked`, `expires_at`) VALUES
('3c999cf388289b8aa77df09b591d8b9a5deecf91c63e05eef1d7906078fd85264902467f86952dd6', 'b6bf2e11c6812c6a5b1f3e6ed1fbf9f561afc59a17f46485cd22fd253c94b74055b34e180293eaf0', 0, '2021-09-23 12:39:45'),
('65e7e416dda8cc47bc49d2c40c5eacf1933702d029ad976ba6db04116c5f2e67188f3775d4008b5d', '4f1f04610fd69b665fe1b80ff77108fe6e1e50a445e3d72ee8cd575273664153adb1bada0790af0a', 0, '2021-09-22 20:45:22'),
('7b25a69f5205c15b6ee469eeae79346ffbf63dc6c420d124946158d8c330392e5653932be331b1f5', '3c952f013e182bc0ed3f1efeba2e4e575a122c9662913a3ff24d2a79fe2f90631a5faa11fa1a04f8', 0, '2021-09-22 12:06:37'),
('a1470075727af511874c783e9b664039d23536174dd924ead3071fe1f19147a25908607c1d5eb9ff', 'a966707f56445f19df4fd21f1e54a7e0256ef030348480b074ecb7626f5fe37cdb47acb2d3e36d8e', 0, '2021-09-22 11:54:34'),
('bea2f7f701a6cae4a60f9201dedd7648bbe533f7dcb3241985c59fc2cc45b1ff2c250532e2723a12', 'b1ebc0516381989b0f0aeac9af151e37876258178966e178fc07e9d4b8fa43f5c9a480879bbaaa28', 0, '2021-09-22 11:57:18'),
('cfb03cc04a58c6e901013e591913660f018e58090087b93f7c7c03a15868f712873a0e86029e34dc', 'd3b83892aeedd451d391762fee30e75a7115929595d00526f3d18ee3713a2eb9ccf4faffc1ed9e51', 0, '2021-09-22 13:10:45');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tweets`
--

CREATE TABLE `tweets` (
  `id` int(10) UNSIGNED NOT NULL,
  `english_description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `arabic_description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tweets`
--

INSERT INTO `tweets` (`id`, `english_description`, `arabic_description`, `user_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Hello this is first tweet update', NULL, 1, '2020-09-22 18:50:03', '2020-09-23 10:40:05', '2020-09-23 10:40:05'),
(2, 'Hello this is second tweet created', NULL, 2, '2020-09-22 18:50:03', '2020-09-22 18:56:43', NULL),
(3, 'Hello this is third tweet created', NULL, 1, '2020-09-22 18:50:03', '2020-09-22 18:56:43', NULL),
(4, 'Hello this is fourth tweet created', NULL, 4, '2020-09-22 18:50:03', '2020-09-22 18:56:43', NULL),
(5, NULL, 'لوريم ايبسوم هو نموذج افتراضي يوضع في التصاميم', 1, '2020-09-22 19:00:04', '2020-09-22 19:00:04', NULL),
(6, NULL, 'وعند موافقه العميل المبدئيه على التصميم يتم ازالة هذا النص ', 2, '2020-09-22 19:00:04', '2020-09-22 19:00:04', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `auth_client_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`, `image`, `deleted_at`, `auth_client_id`) VALUES
(1, 'Nesma', 'nesmamohamed94@gmail.com', NULL, '$2y$10$k1m9p5vwDHF.QmEjH8IMYOiT32mgzR7BjVTfIA9bQc2JNdx9JGURC', NULL, '2020-09-22 09:41:35', '2020-09-22 11:17:49', 'http://localhost/tasks/public/images/download.jpg', NULL, 2),
(2, 'Nesma Mohamed', 'nesmamohamed@gmail.com', NULL, '$2y$10$k1m9p5vwDHF.QmEjH8IMYOiT32mgzR7BjVTfIA9bQc2JNdx9JGURC', NULL, '2020-09-22 09:41:35', '2020-09-22 10:53:12', 'http://localhost/tasks/public/images/download.jpg', NULL, 2),
(3, 'Nesma Mohamed', 'nesma@gmail.com', NULL, '$2y$10$k1m9p5vwDHF.QmEjH8IMYOiT32mgzR7BjVTfIA9bQc2JNdx9JGURC', NULL, '2020-09-22 09:41:35', '2020-09-22 10:53:12', 'http://localhost/tasks/public/images/download.jpg', NULL, 2),
(4, 'Ahmed', 'Ahmed@gmail.com', NULL, '$2y$10$8T9MOgJqHkQWm5/UQvOMXucEwC36BnktY4CaOekp356errtBqKL6m', NULL, '2020-09-22 11:20:46', '2020-09-22 11:20:46', 'http://localhost/tasks/public/images/download.jpg', NULL, 2);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `followers`
--
ALTER TABLE `followers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_access_tokens`
--
ALTER TABLE `oauth_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_access_tokens_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_auth_codes`
--
ALTER TABLE `oauth_auth_codes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_auth_codes_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_clients_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_refresh_tokens`
--
ALTER TABLE `oauth_refresh_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `tweets`
--
ALTER TABLE `tweets`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `tweets_id_unique` (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `followers`
--
ALTER TABLE `followers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tweets`
--
ALTER TABLE `tweets`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
